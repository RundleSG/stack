# Stack Distribution

## Usage Instructions
1. Clone and unpack latest Drupal 8 release
2. Clone and unpack latest Stack release
3. Edit and confirm that composer.json requires latest Drupal version
4. Run `composer install` This replaces Drupal's default composer.json and will automatically install Stack dependencies

### Stack Distribution Contains
* Libraries
* Themes
* composer.json - used for dependencies 
* .htaccess (may want to use the default drupal .htaccess)

#### V1.0.0 
* Initial Release

#### V1.0.1
* Fixes TopPlus Lite CSS - There was a version mismatch when initially exported